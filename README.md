Times Geodata models
=========================

This is a private repository to make models related to the times_geodata db available to multiple apps.

Requirements
=========================
* PHP >= 5.6.4
* Laravel >= 5.3

Features
--------

None. Just a bunch of model classes. If you want to know how to use it, please contact Joyce at joyce@indzz.com. Otherwise it should be pretty straightforward.
