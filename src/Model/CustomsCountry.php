<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomsCountry
 *
 * @property int $id
 * @property int $country_id
 * @property string $country_code
 * @property string|null $country_cn_name
 * @property string|null $country_en_name
 * @property string|null $iso_e
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomsCountry onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsCountry whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsCountry whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsCountry whereCountryCnName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsCountry whereCountryEnName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsCountry whereIsoE($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsCountry whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsCountry whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomsCountry withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomsCountry withoutTrashed()
 * @mixin \Eloquent
 */
class CustomsCountry extends Model
{
    protected $table = "customs_countries";
    protected $connection = 'geodb_mysql';
    protected $fillable = [];
}
