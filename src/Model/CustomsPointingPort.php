<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomsPointingPort
 *
 * @property int $id
 * @property string $code
 * @property string|null $en_name
 * @property string|null $cn_name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomsPointingPort onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsPointingPort whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsPointingPort whereCnName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsPointingPort whereEnName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsPointingPort whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsPointingPort whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomsPointingPort withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomsPointingPort withoutTrashed()
 * @mixin \Eloquent
 */
class CustomsPointingPort extends Model
{
    protected $table = "Customs_pointing_port";
    protected $connection = 'geodb_mysql';
    protected $fillable = [];
}
