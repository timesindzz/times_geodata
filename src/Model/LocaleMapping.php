<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\LocaleMapping
 *
 * @property int $id
 * @property int $country_id
 * @property int $level
 * @property string $search_str
 * @property string $replace_str
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Country $country
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LocaleMapping onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocaleMapping whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocaleMapping whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocaleMapping whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocaleMapping whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocaleMapping whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocaleMapping whereReplaceStr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocaleMapping whereSearchStr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LocaleMapping whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LocaleMapping withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LocaleMapping withoutTrashed()
 * @mixin \Eloquent
 */
class LocaleMapping extends Model
{
    use SoftDeletes;

    protected $table = "locale_mappings";

    protected $connection = 'geodb_mysql';

    protected $fillable = ['geolevel_type', 'search_str', 'replace_str', 'country_id'];

    public function country() {
        return $this->belongsTo(Country::class);
    }

}
