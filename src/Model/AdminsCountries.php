<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class AdminsCountries extends Model
{
    use SoftDeletes;

    protected $table = "admins_countries";

    protected $connection = 'geodb_mysql';

    protected $fillable = ['admin_id','country_id'];


}
