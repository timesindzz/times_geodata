<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Country
 *
 * @property int $id
 * @property string $name
 * @property string|null $alt_name
 * @property string $code
 * @property int $geocode_required
 * @property string|null $lang_codes
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FirstLevelDivision[] $firstLevels
 * @property-read \App\Models\GeocodeMapping $geocodeMapping
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\LastmileProvider[] $lastmileProviders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\LocaleMapping[] $localeMappings
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Country onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereAltName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereGeocodeRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereLangCodes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Country whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Country withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Country withoutTrashed()
 * @mixin \Eloquent
 */
class Country extends Model
{
    use SoftDeletes;

    protected $table = "countries";

    protected $connection = 'geodb_mysql';

    protected $fillable = ['name', 'alt_name', 'code'];

    public function lastmileProviders() {
        return $this->hasMany(LastmileProvider::class)->orderBy('priority');
    }

    public function firstLevels(){
      return $this->hasMany(FirstLevelDivision::class);
    }

    public function localeMappings(){
      return $this->hasMany(LocaleMapping::class);
    }

    public function geocodeMappings(){
      return $this->hasMany(GeocodeMapping::class);
    }

}
