<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomsUnit
 *
 * @property int $id
 * @property string $unit_code
 * @property string|null $unit_cn_name
 * @property string|null $unit_en_name
 * @property string|null $country_en_name
 * @property string|null $unit_conv_code
 * @property string|null $unit_conv_ratio
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomsUnit onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsUnit whereUnitCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsUnit whereUnitCnName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsUnit whereUnitEnName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsUnit whereUnitConvCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsUnit whereUnitConvRatio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsUnit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsUnit whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomsUnit withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomsUnit withoutTrashed()
 * @mixin \Eloquent
 */
class CustomsUnit extends Model
{
    protected $table = "customs_units";
    protected $connection = 'geodb_mysql';
    protected $fillable = [];
}
