<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomsCurrency
 *
 * @property int $id
 * @property string $currency_code
 * @property string|null $currency_symb
 * @property string|null $currency_cn_name
 * @property string|null $currency_en_name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomsCurrency onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsCurrency whereCurrencyCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsCurrency whereCurrencySymb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsCurrency whereCurrencyEnName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsCurrency whereCurrencyCnName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsCurrency whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomsCurrency whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomsCurrency withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomsCurrency withoutTrashed()
 * @mixin \Eloquent
 */
class CustomsCurrency extends Model
{
    protected $table = "customs_currency";
    protected $connection = 'geodb_mysql';
    protected $fillable = [];
}
