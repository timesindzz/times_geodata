<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\GeocodeMapping
 *
 * @property int $id
 * @property int|null $country_id
 * @property string|null $first_level_attribute
 * @property string|null $second_level_attribute
 * @property string|null $third_level_attribute
 * @property string|null $fourth_level_attribute
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Country|null $country
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\GeocodeMapping onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GeocodeMapping whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GeocodeMapping whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GeocodeMapping whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GeocodeMapping whereFirstLevelAttribute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GeocodeMapping whereFourthLevelAttribute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GeocodeMapping whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GeocodeMapping whereSecondLevelAttribute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GeocodeMapping whereThirdLevelAttribute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GeocodeMapping whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\GeocodeMapping withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\GeocodeMapping withoutTrashed()
 * @mixin \Eloquent
 */
class GeocodeMapping extends Model
{
    use SoftDeletes;

    protected $table = "geocode_mappings";

    protected $connection = 'geodb_mysql';

    protected $fillable = ['country_id','first_level_attribute','second_level_attribute','third_level_attribute','fourth_level_attribute'];

    public function country() {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

}
