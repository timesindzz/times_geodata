<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\LastmileProvider
 *
 * @property int $id
 * @property int|null $country_id
 * @property string $provider_name
 * @property string $status
 * @property int $priority
 * @property int $lowest_level
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Country|null $country
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LastmileProvider onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LastmileProvider whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LastmileProvider whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LastmileProvider whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LastmileProvider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LastmileProvider whereLowestLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LastmileProvider wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LastmileProvider whereProviderName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LastmileProvider whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LastmileProvider whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LastmileProvider withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LastmileProvider withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrackingStatus[] $trackingStatuses
 */
class LastmileProvider extends Model
{
    use SoftDeletes;

    protected $table = "lastmile_providers";

    protected $connection = 'geodb_mysql';

    protected $fillable = ['provider_name', 'status', 'priority', 'country_id'];

    public function country() {
        return $this->belongsTo(Country::class);
    }

    public function trackingStatuses() {
        return $this->hasMany(TrackingStatus::class);
    }

    public function statusUpdates() {
        return $this->hasMany(StatusUpdate::class);
    }

}
