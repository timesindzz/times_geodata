<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TrackStatusSetting
 * @package Imjoyce\TimesGeodata\Model
 */
class TrackStatusSetting extends Model
{
    use SoftDeletes;

    protected $table = "track_status_settings";

    protected $connection = 'geodb_mysql';

    protected $fillable = ['lastmile_provider_id', 'status_group_id', 'set_name'];

}
