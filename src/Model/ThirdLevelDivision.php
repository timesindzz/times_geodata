<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\ThirdLevelDivision
 *
 * @property int $id
 * @property int $second_level_division_id
 * @property string|null $name
 * @property string $name_locale
 * @property string|null $service_code
 * @property string|null $zone
 * @property int $can_deliver
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FourthLevelDivision[] $fourthlevels
 * @property-read \App\Models\SecondLevelDivision $secondlevel
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ThirdLevelDivision onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdLevelDivision whereCanDeliver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdLevelDivision whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdLevelDivision whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdLevelDivision whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdLevelDivision whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdLevelDivision whereNameLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdLevelDivision whereSecondLevelDivisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdLevelDivision whereServiceCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdLevelDivision whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdLevelDivision whereZone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ThirdLevelDivision withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ThirdLevelDivision withoutTrashed()
 * @mixin \Eloquent
 */
class ThirdLevelDivision extends Model
{
    use SoftDeletes;

    protected $table = "third_level_divisions";

    protected $connection = 'geodb_mysql';

    protected $fillable = ['second_level_division_id', 'name', 'name_locale', 'service_code', 'zone', 'can_deliver'];

    public function secondlevel() {
        return $this->belongsTo(SecondLevelDivision::class, 'second_level_division_id', 'id');
    }

    public function fourthlevels(){
      return $this->hasMany(FourthLevelDivision::class, 'third_level_division_id', 'id');
    }



}
