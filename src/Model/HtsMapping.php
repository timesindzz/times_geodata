<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Imjoyce\TimesGeodata\Model\Country;
use Imjoyce\TimesGeodata\Model\LastmileProvider;


class HtsMapping extends Model
{
    use SoftDeletes;

    protected $table = "hts_mappings";

    protected $connection = 'geodb_mysql';

    protected $fillable = ['id', 'country_id', 'lastmile_provider_id', 'hts_code', 'description', 'commodity_id'];

    public function country() {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function service_provider() {
        return $this->belongsTo(LastmileProvider::class, 'lastmile_provider_id', 'id');
    }
}
