<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Customs
 *
 * @property int $id
 * @property string $customs_code
 * @property string|null $customs_cn_name
 * @property string|null $customs_en_name
 * @property string|null $abbr_cust_cn
 * @property string|null $abbr_cust_en
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Customs onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customs whereCustomsCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customs whereCustomsCnName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customs whereCustomsEnName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customs whereAbbrCustCn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customs whereAbbrCustEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customs whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Customs whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Customs withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Customs withoutTrashed()
 * @mixin \Eloquent
 */
class Customs extends Model
{
    protected $table = "customs";
    protected $connection = 'geodb_mysql';
    protected $fillable = [];
}
