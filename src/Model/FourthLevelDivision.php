<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\FourthLevelDivision
 *
 * @property int $id
 * @property int $third_level_division_id
 * @property string|null $name
 * @property string $name_locale
 * @property string|null $service_code
 * @property string|null $zone
 * @property int $can_deliver
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\ThirdLevelDivision $thirdlevel
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FourthLevelDivision onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FourthLevelDivision whereCanDeliver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FourthLevelDivision whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FourthLevelDivision whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FourthLevelDivision whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FourthLevelDivision whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FourthLevelDivision whereNameLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FourthLevelDivision whereServiceCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FourthLevelDivision whereThirdLevelDivisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FourthLevelDivision whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FourthLevelDivision whereZone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FourthLevelDivision withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FourthLevelDivision withoutTrashed()
 * @mixin \Eloquent
 */
class FourthLevelDivision extends Model
{
    use SoftDeletes;

    protected $table = "fourth_level_divisions";

    protected $connection = 'geodb_mysql';

    protected $fillable = ['third_level_division_id', 'name', 'name_locale', 'service_code', 'zone', 'can_deliver'];

    public function thirdlevel() {
        return $this->belongsTo(ThirdLevelDivision::class, 'third_level_division_id', 'id');
    }

}
