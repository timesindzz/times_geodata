<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\FirstLevelDivision
 *
 * @property int $id
 * @property int $country_id
 * @property string|null $name
 * @property string $name_locale
 * @property string|null $service_code
 * @property string|null $zone
 * @property int $can_deliver
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Country $country
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SecondLevelDivision[] $secondLevels
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FirstLevelDivision onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FirstLevelDivision whereCanDeliver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FirstLevelDivision whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FirstLevelDivision whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FirstLevelDivision whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FirstLevelDivision whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FirstLevelDivision whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FirstLevelDivision whereNameLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FirstLevelDivision whereServiceCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FirstLevelDivision whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FirstLevelDivision whereZone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FirstLevelDivision withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\FirstLevelDivision withoutTrashed()
 * @mixin \Eloquent
 */
class FirstLevelDivision extends Model
{
    use SoftDeletes;

    protected $table = "first_level_divisions";

    protected $connection = 'geodb_mysql';

    protected $fillable = ['name', 'name_locale', 'service_code', 'zone', 'can_deliver', 'country_id'];

    public function country() {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function secondLevels(){
      return $this->hasMany(SecondLevelDivision::class, 'first_level_division_id', 'id');
    }

}
