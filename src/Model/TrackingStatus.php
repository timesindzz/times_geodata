<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\TrackingStatus
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int|null $lastmile_provider_id
 * @property string $order_status
 * @property string $message
 * @property string $timestamp_field
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\LastmileProvider|null $lastmileProvider
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TrackingStatus onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrackingStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrackingStatus whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrackingStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrackingStatus whereLastmileProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrackingStatus whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrackingStatus whereOrderStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrackingStatus whereTimestampField($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TrackingStatus whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TrackingStatus withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TrackingStatus withoutTrashed()
 */
class TrackingStatus extends Model
{
    use SoftDeletes;

    protected $table = "tracking_statuses";

    protected $connection = 'geodb_mysql';

    protected $fillable = ['order_status', 'message', 'timestamp_field'];

    public function lastmileProvider()
    {
        return $this->belongsTo(LastmileProvider::class);
    }
}
