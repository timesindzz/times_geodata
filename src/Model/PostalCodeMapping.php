<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Imjoyce\TimesGeodata\Model\Country;
use Imjoyce\TimesGeodata\Model\LastmileProvider;

/**
 * Imjoyce\TimesGeodata\Model\PostalCodeMapping
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $country_id
 * @property string $postal_code
 * @property string|null $first_level_name
 * @property string $first_level_name_locale
 * @property string|null $second_level_name
 * @property string|null $second_level_name_locale
 * @property string|null $third_level_name
 * @property string|null $third_level_name_locale
 * @property string|null $fourth_level_name
 * @property string|null $fourth_level_name_locale
 * @property string|null $service_code
 * @property int $can_deliver
 * @property int|null $lastmile_provider_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Imjoyce\TimesGeodata\Model\Country $country
 * @property-read \Imjoyce\TimesGeodata\Model\LastmileProvider|null $service_provider
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping whereCanDeliver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping whereFirstLevelName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping whereFirstLevelNameLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping whereFourthLevelName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping whereFourthLevelNameLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping whereLastmileProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping whereSecondLevelName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping whereSecondLevelNameLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping whereServiceCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping whereThirdLevelName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping whereThirdLevelNameLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Imjoyce\TimesGeodata\Model\PostalCodeMapping withoutTrashed()
 */
class PostalCodeMapping extends Model
{
    use SoftDeletes;

    protected $table = "postal_code_mappings";

    protected $connection = 'geodb_mysql';

    protected $fillable = ['id', 'country_id', 'postal_code', 'first_level_name', 'first_level_name_locale', 'second_level_name', 'second_level_name_locale', 'third_level_name', 'third_level_name_locale',
        'fourth_level_name', 'fourth_level_name_locale', 'service_code', 'can_deliver', 'lastmile_provider_id'];

    public function country() {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function service_provider() {
        return $this->belongsTo(LastmileProvider::class, 'lastmile_provider_id', 'id');
    }
}
