<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ApiInfo extends Model
{
    use SoftDeletes;

    protected $table = "api_info";

    protected $connection = 'geodb_mysql';

    protected $fillable = [
                            'provider_name',
                            'field_name',
                            'field_value',
                            ];

}
