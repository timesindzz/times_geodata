<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderSpecialNo extends Model
{
    use SoftDeletes;

    protected $table = "provider_special_no";

    protected $connection = 'geodb_mysql';

    protected $guarded = [];

}
