<?php

namespace Imjoyce\TimesGeodata\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\SecondLevelDivision
 *
 * @property int $id
 * @property int $first_level_division_id
 * @property string|null $name
 * @property string $name_locale
 * @property string|null $service_code
 * @property string|null $zone
 * @property int $can_deliver
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\FirstLevelDivision $firstLevel
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ThirdLevelDivision[] $thirdLevels
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SecondLevelDivision onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecondLevelDivision whereCanDeliver($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecondLevelDivision whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecondLevelDivision whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecondLevelDivision whereFirstLevelDivisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecondLevelDivision whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecondLevelDivision whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecondLevelDivision whereNameLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecondLevelDivision whereServiceCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecondLevelDivision whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecondLevelDivision whereZone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SecondLevelDivision withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SecondLevelDivision withoutTrashed()
 * @mixin \Eloquent
 */
class SecondLevelDivision extends Model
{
    use SoftDeletes;

    protected $table = "second_level_divisions";

    protected $connection = 'geodb_mysql';

    protected $fillable = ['first_level_division_id', 'name', 'name_locale', 'service_code', 'zone', 'can_deliver'];

    public function firstLevel() {
        return $this->belongsTo(FirstLevelDivision::class, 'first_level_division_id', 'id');
    }

    public function thirdLevels(){
      return $this->hasMany(ThirdLevelDivision::class, 'second_level_division_id', 'id');
    }



}
